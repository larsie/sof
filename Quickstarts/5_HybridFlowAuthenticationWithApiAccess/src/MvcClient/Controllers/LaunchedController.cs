﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MvcClient.Controllers
{
    public class LaunchedController : Controller
    {
        // GET: Launched
        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Index(string code, string state)
        {
            ViewBag.code = code;
            ViewBag.state = state;
            //return Content($"Code={code} state={state}");
            return View();
        }
    }
}